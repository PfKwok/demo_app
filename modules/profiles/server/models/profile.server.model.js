'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Profile Schema
 */
var ProfileSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill player name',
    trim: true
  },
  pnumber: {
    type: Number,
    default: '',
    required: 'Please fill player number'
  },
  team: {
    type: String,
    default: '',
    required: 'Please fill player team',
    trim: true
  },
  body: {
    type: String,
    default: '',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Profile', ProfileSchema);
